﻿using System.Threading.Tasks;

namespace Calculator.Domain
{
    public class TaxService
    {
        private readonly ITaxCalculator taxCalculator;
        public TaxService(ITaxCalculator taxCalculator) => 
            this.taxCalculator = taxCalculator;
        public Task<LocationTaxRates> GetTaxRatesAsync(Location location) =>
            taxCalculator.GetTaxRatesAsync(location);

        public Task<OrderTax> CalculateTaxAsync(Order order) =>
            taxCalculator.CalculateTaxAsync(order);
    }
}
