﻿namespace Calculator.Domain
{
    public class OrderTax
    {
        public decimal TaxCollectable { get; set; }

        public bool FreightTaxable { get; set; }

        public decimal OrderTotal { get; set; }

        public decimal Shipping { get; set; }

        public decimal TaxableAmount { get; set; }

        public decimal TaxRate { get; set; }

        public string TaxSource { get; set; }
    }
}
