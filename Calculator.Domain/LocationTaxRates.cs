﻿namespace Calculator.Domain
{
    public class LocationTaxRates
    {
        public decimal StateTaxRate { get; set; }
        public decimal CountyTaxRate { get; set; }
        public decimal CityTaxRate { get; set; }
        public decimal CombinedDistrictTaxRate { get; set; }
        public decimal CombinedTaxRate { get; set; }
        public bool FreightTaxable { get; set; }
    }
}
