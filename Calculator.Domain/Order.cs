﻿namespace Calculator.Domain
{
    public class Order
    {
        public Location From { get; set; }
        public Location To { get; set; }
        public decimal Amount { get; set; }
        public decimal Shipping { get; set; }
    }
}
