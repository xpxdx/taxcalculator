﻿using System.Threading.Tasks;

namespace Calculator.Domain
{
    public interface ITaxCalculator
    {
        Task<OrderTax> CalculateTaxAsync(Order order);
        Task<LocationTaxRates> GetTaxRatesAsync(Location location);
    }
}