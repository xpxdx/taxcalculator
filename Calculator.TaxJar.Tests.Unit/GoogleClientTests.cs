using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using Calculator.TaxJar;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calculator.Client.TaxJar.Test.Unit
{
    public class GoogleClientTests
    {
        [SetUp]
        public void Setup()
        {
        }


        //[Test]
        public void ExecuteAsyncWithMoq()
        {
            var restClient = new Mock<IRestClient>();

            restClient
                .Setup(x => x.ExecuteAsync(
                    It.IsAny<IRestRequest>(),
                    It.IsAny<Action<IRestResponse, RestRequestAsyncHandle>>()))
                .Callback<IRestRequest, Action<IRestResponse, RestRequestAsyncHandle>>((request, callback) =>
                {
                    callback(new RestResponse { StatusCode = HttpStatusCode.OK }, null);
                });

            restClient.Object.ExecuteAsync(new RestRequest(), (response, handle) =>
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
        }

        //[Test]
        public async Task GetGoogleAsync3()
        {
            var expected = "response content";
            var expectedBytes = Encoding.UTF8.GetBytes(expected);

            var restClient = new Mock<IRestClient>();
            restClient
                .Setup(x => x.ExecuteAsync(
                    It.IsAny<IRestRequest>(), 
                    It.IsAny<Action<IRestResponse, RestRequestAsyncHandle>>()))
                .Callback<IRestRequest, Action<IRestResponse, RestRequestAsyncHandle>>((request, callback) =>
                {
                    callback(new RestResponse { StatusCode = HttpStatusCode.OK, Content = expected }, null);
                });

            // act
            var clientService = new GoogleClient(restClient.Object);
            var actualResponse = await clientService.GetGoogle();
        }

        [Test]
        public async Task Collection_UnitTest()
        {
            var expected = "response content";

            var mock = new Mock<IRestClient>();
            mock.Setup(x => x.ExecuteAsync(It.IsAny<RestRequest>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new RestResponse
            {
                StatusCode = HttpStatusCode.OK,
                Content = expected,
            });

            var clientService = new GoogleClient(mock.Object);
            var actualResponse = await clientService.GetGoogle();

            Assert.AreEqual(actualResponse, expected);
        }

        //[Test]
        public async Task GetGoogleAsync4()
        {
            var expected = "response content";
            var expectedBytes = Encoding.UTF8.GetBytes(expected);

            var restClient = new Mock<IRestClient>();
            restClient
                .Setup(x => x.ExecuteAsync(
                    It.IsAny<IRestRequest>(),
                    It.IsAny<Action<IRestResponse, RestRequestAsyncHandle>>()))
                .Callback<IRestRequest, Action<IRestResponse, RestRequestAsyncHandle>>((request, callback) =>
                {
                    callback(new RestResponse { StatusCode = HttpStatusCode.OK, Content = expected }, null);
                });

            // act
            var clientService = new GoogleClient(restClient.Object);
            var actualResponse = await clientService.GetGoogle();
        }

        public static IRestClient MockRestClient2<T>(HttpStatusCode httpStatusCode, string json)
        {
            var mockIRestClient = new Mock<IRestClient>();
            mockIRestClient.Setup(x => x.Execute<T>(It.IsAny<IRestRequest>()))
              .Returns(new RestResponse<T>
              {
                  Data = JsonConvert.DeserializeObject<T>(json),
                  StatusCode = httpStatusCode
              });
            return mockIRestClient.Object;
        }

        public static IRestClient MockRestClient<T>(HttpStatusCode httpStatusCode, string json) where T : new()
        {
            var data = JsonConvert.DeserializeObject<T>(json);
            var response = new Mock<IRestResponse<T>>();

            response.Setup(r => r.StatusCode).Returns(httpStatusCode);
            response.Setup(r => r.Data).Returns(data);

            var mockClient = new Mock<IRestClient>();
            mockClient.Setup(x => x.Execute<T>(It.IsAny<IRestRequest>())).Returns(response.Object);

            return mockClient.Object;
        }

        //[Test]
        //public async Task GetUserByUserName()
        //{
        //    //Arrange
        //    var client = MockRestClient<User>(HttpStatusCode.OK, "my json code");
        //    var dataServices = new GoogleClient(client);
        //    //Act
        //    var user = await dataServices.GetUserByUserName("User1");
        //    //Assert
        //    Assert.AreEqual("User1", user.Username);
        //}



        //[Test]
        //public async void GetGoogleAsync2()
        //{
        //    var mockIRestClient = new Mock<IRestClient>();
        //    mockIRestClient.Setup(x => x.ExecuteAsync(request: It.IsAny<IRestRequest>()), Method.POST, null)
        //        .Returns(new RestResponse
        //        {
        //            StatusCode = HttpStatusCode.OK, 
        //            //Data = null,
        //            RawBytes = Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"UTF-8\"?><error code=\"100\" description=\"Incorrect user credentials\"/>")
        //        });
        //}

        //[Test]
        public async void GetGoogleAsync_Returns_Not_Empty_Value()
        {
            var client = new RestClient();

            var google = new GoogleClient(client);
            string result = await google.GetGoogle();
            Assert.IsTrue(true);
        }

        //[Test]
        public void Test1()
        {
            JObject o = JObject.Parse(@"{
              'tax': {
                'order_total_amount': 16.5,
                'shipping': 1.5,
                'taxable_amount': 15,
                'amount_to_collect': 1.35,
                'rate': 0.09,
                'has_nexus': true,
                'freight_taxable': false,
                'tax_source': 'destination',
                'jurisdictions': {
                  'country': 'US',
                  'state': 'CA',
                  'county': 'LOS ANGELES',
                  'city': 'LOS ANGELES'
                },
                'breakdown': {
                  'taxable_amount': 15,
                  'tax_collectable': 1.35,
                  'combined_tax_rate': 0.09,
                  'state_taxable_amount': 15,
                  'state_tax_rate': 0.0625,
                  'state_tax_collectable': 0.94,
                  'county_taxable_amount': 15,
                  'county_tax_rate': 0.0025,
                  'county_tax_collectable': 0.04,
                  'city_taxable_amount': 0,
                  'city_tax_rate': 0,
                  'city_tax_collectable': 0,
                  'special_district_taxable_amount': 15,
                  'special_tax_rate': 0.025,
                  'special_district_tax_collectable': 0.38,
                  'line_items': [
                    {
                      'id': '1',
                      'taxable_amount': 15,
                      'tax_collectable': 1.35,
                      'combined_tax_rate': 0.09,
                      'state_taxable_amount': 15,
                      'state_sales_tax_rate': 0.0625,
                      'state_amount': 0.94,
                      'county_taxable_amount': 15,
                      'county_tax_rate': 0.0025,
                      'county_amount': 0.04,
                      'city_taxable_amount': 0,
                      'city_tax_rate': 0,
                      'city_amount': 0,
                      'special_district_taxable_amount': 15,
                      'special_tax_rate': 0.025,
                      'special_district_amount': 0.38
                    }
                  ]
                }
              }
            }");

            var tax = (decimal)o.SelectToken("tax.amount_to_collect");
            // Acme Co

            //decimal productPrice = (decimal)o.SelectToken("Manufacturers[0].Products[0].Price");
            //// 50

            //string productName = (string)o.SelectToken("Manufacturers[1].Products[0].Name");
            //// Elbow Grease

            Assert.Pass();
        }
    }
}