﻿using Calculator.Domain;
using Moq;
using NUnit.Framework;
using RestSharp;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Calculator.TaxJar.Tests.Unit
{
    class TaxJarTaxCalculatorTests
    {
        [Test]
        public async Task GetTaxRatesAsync_Method_Should_Return_TaxRates_Specified_By_Moq_Json_Response()
        {
            //Setup
            var content = @"{
              'rate': {
                'zip': '90404',
                'state': 'CA',
                'state_rate': '0.0625',
                'county': 'LOS ANGELES',
                'county_rate': '0.01',
                'city': 'SANTA MONICA',
                'city_rate': '0.0',
                'combined_district_rate': '0.025',
                'combined_rate': '0.0975',
                'freight_taxable': false
              }
            }";

            var moqClient = new Mock<IRestClient>();

            moqClient.Setup(x => x.ExecuteAsync(It.IsAny<RestRequest>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new RestResponse
            {
                StatusCode = HttpStatusCode.OK,
                Content = content
            });

            var moqConfig = new Mock<ITaxJarConfiguration>();
            moqConfig.Setup(x => x.ApiToken).Returns("1234567890");
            moqConfig.Setup(x => x.BaseUrl).Returns("https://nosuchurl.com");

            var expected = new LocationTaxRates
            {
                StateTaxRate = 0.0625m,
                CountyTaxRate = 0.01m,
                CityTaxRate = 0.0m,
                CombinedDistrictTaxRate = 0.025m,
                CombinedTaxRate = 0.0975m,
                FreightTaxable = false
            };

            //Do
            var SUT = new TaxJarTaxCalculator(moqClient.Object, moqConfig.Object);
            var actual = await SUT.GetTaxRatesAsync(new Location());

            //Assert
            AssertAreEqual(actual, expected);
        }

        [Test]
        public async Task CalculateTaxAsync_Method_Should_Return_TaxCalculation_Specified_By_Moq_Json_Response()
        {
            //Setup
            var moqResponse = @"{
	            'tax': {
		            'amount_to_collect': 1.19,
		            'freight_taxable': true,
		            'has_nexus': true,
		            'jurisdictions': {
			            'city': 'RAMSEY',
			            'country': 'US',
			            'county': 'BERGEN',
			            'state': 'NJ'
		            },
		            'order_total_amount': 18.0,
		            'rate': 0.06625,
		            'shipping': 1.5,
		            'tax_source': 'destination',
		            'taxable_amount': 18.0
	            }
            }";

            var moqClient = new Mock<IRestClient>();
            moqClient.Setup(x => x.ExecuteAsync(It.IsAny<RestRequest>(), It.IsAny<Method>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new RestResponse
            {
                StatusCode = HttpStatusCode.OK,
                Content = moqResponse
            });

            var moqConfig = new Mock<ITaxJarConfiguration>();
            moqConfig.Setup(x => x.ApiToken).Returns("123456789");
            moqConfig.Setup(x => x.BaseUrl).Returns("https://google.com");

            var expected = new OrderTax
            {
                TaxCollectable = 1.19m,
                FreightTaxable = true,
                OrderTotal = 18.0m,
                TaxRate = 0.06625m,
                Shipping = 1.5m,
                TaxSource = "destination",
                TaxableAmount = 18.0m
            };

            var order = new Order
            {
                From = new Location
                {
                    Country = "US",
                    Zip = "92093",
                    State = "CA",
                    City = "La Jolla",
                    Street = "9500 Gilman Drive"
                },
                To = new Location
                {
                    Country = "US",
                    Zip = "90002",
                    State = "CA",
                    City = "Los Angeles",
                    Street = "1335 E 103rd St"
                },
                Amount = 15m,
                Shipping = 1.5m
            };

            //Do
            var SUT = new TaxJarTaxCalculator(moqClient.Object, moqConfig.Object);
            var actual = await SUT.CalculateTaxAsync(order);

            //Assert
            AssertAreEqual(actual, expected);
        }

        public static void AssertAreEqual<T>(T expected, T actual)
        {
            if (expected == null && actual == null)
            {
                Assert.IsTrue(true);
            }
            else if (expected == null && actual != null || expected != null && actual == null)
            {
                Assert.Fail("One of two objects is null");
            }
            else
            {
                var type = typeof(T);
                foreach (var property in type.GetProperties())
                {
                    var expectedValue = property.GetValue(expected, null);
                    var actualValue = property.GetValue(actual, null);
                    if (expectedValue == null && actualValue == null)
                        continue;
                    Assert.AreEqual(expectedValue, actualValue);
                }
            }
        }
    }
}
