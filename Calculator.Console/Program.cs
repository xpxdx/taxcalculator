﻿using Calculator.Domain;
using Calculator.TaxJar;
using RestSharp;
using System;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace CalculatorRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();
            Console.ReadLine();
        }
        static async Task MainAsync()
        {
            var taxService = ConfigureTaxService();

            var location1 = new Location
            {
                Country = "US",
                Zip = "92093",
                State = "CA",
                City = "La Jolla",
                Street = "9500 Gilman Drive"
            };
            await ShowTaxRatesForLocation(location1, taxService);

            var location2 = new Location
            {
                Country = "US",
                Zip = "92093"
            };
            await ShowTaxRatesForLocation(location2, taxService);

            var order1 = new Order
            {
                From = new Location
                {
                    Country = "US",
                    Zip = "92093",
                    State = "CA",
                    City = "La Jolla",
                    Street = "9500 Gilman Drive"
                },
                To = new Location
                {
                    Country = "US",
                    Zip = "90002",
                    State = "CA",
                    City = "Los Angeles",
                    Street = "1335 E 103rd St"
                },
                Amount = 15m,
                Shipping = 1.5m
            };
            await ShowTaxesForOrder(order1, taxService);

            var order2 = new Order
            {
                From = new Location
                {
                    Country = "US",
                    Zip = "92093",
                    State = "CA"
                },
                To = new Location
                {
                    Country = "US",
                    Zip = "90002",
                    State = "CA"
                },
                Amount = 15m,
                Shipping = 1.5m
            };
            await ShowTaxesForOrder(order2, taxService);
        }

        static TaxService ConfigureTaxService()
        {
            var calcConfig = new TaxJarConfiguration();
            var restClient = new RestClient();
            var taxCalculator = new TaxJarTaxCalculator(restClient, calcConfig);
            return new TaxService(taxCalculator);
        }

        static async Task ShowTaxRatesForLocation(Location location, TaxService taxService)
        {
            Console.WriteLine("***Getting location tax rates for:***");
            DumpAsYaml(location);

            var rates = await taxService.GetTaxRatesAsync(location);

            Console.WriteLine("***Location tax rates:***");
            DumpAsYaml(rates);
        }

        static async Task ShowTaxesForOrder(Order order, TaxService taxService)
        {
            Console.WriteLine("***Getting order taxes for:***");
            DumpAsYaml(order);

            var taxes = await taxService.CalculateTaxAsync(order);

            Console.WriteLine("***Order taxes:***");
            DumpAsYaml(taxes);
        }

        private static void DumpAsYaml(object data)
        {
            var sb = new StringBuilder();
            var sr = new Serializer();
            sb.AppendLine(sr.Serialize(data));
            Console.WriteLine(sb);
            Console.WriteLine("");
        }
    }
}
