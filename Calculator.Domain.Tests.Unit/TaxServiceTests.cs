using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Calculator.Domain.Tests.Unit
{
    public class TaxServiceTests
    {
        [Test]
        public void GetTaxRatesAsync_Method_Should_Pass_Through_Location_TaxRates_Provided_By_ITaxCalculator_Implementation()
        {
            //Setup
            var expected = new LocationTaxRates
            {
                StateTaxRate = 0.0625m,
                CountyTaxRate = 0.01m,
                CityTaxRate = 0.0m,
                CombinedDistrictTaxRate = 0.025m,
                CombinedTaxRate = 0.0975m,
                FreightTaxable = false
            };

            var taxCalculator = new Mock<ITaxCalculator>();
            taxCalculator
                .Setup(c => c.GetTaxRatesAsync(It.IsAny<Location>()))
                .Returns(Task.FromResult(expected));

            //Do
            var SUT = new TaxService(taxCalculator.Object);
            var actual = SUT.GetTaxRatesAsync(new Location()).Result;

            //Assert
            AssertAreEqual(expected, actual);
        }

        [Test]
        public void CalculateTaxAsync_Method_Should_Pass_Through_Order_TaxCalculation_Provided_By_ITaxCalculator_Implementation()
        {
            //Setup
            var expected = new OrderTax
            {
                TaxCollectable = 1.19m,
                FreightTaxable = true,
                OrderTotal = 18.0m,
                TaxRate = 0.06625m,
                Shipping = 1.5m,
                TaxSource = "destination",
                TaxableAmount = 18.0m
            };

            var taxCalculator = new Mock<ITaxCalculator>();
            taxCalculator
                .Setup(c => c.CalculateTaxAsync(It.IsAny<Order>()))
                .Returns(Task.FromResult(expected));

            //Do
            var SUT = new TaxService(taxCalculator.Object);
            var actual = SUT.CalculateTaxAsync(new Order()).Result;

            //Assert
            AssertAreEqual(expected, actual);
        }

        public static void AssertAreEqual<T>(T expected, T actual)
        {
            if (expected == null && actual == null)
            {
                Assert.IsTrue(true);
            }
            else if (expected == null && actual != null || expected != null && actual == null)
            {
                Assert.Fail("One of two objects is null");
            }
            else
            {
                var type = typeof(T);
                foreach (var property in type.GetProperties())
                {
                    var expectedValue = property.GetValue(expected, null);
                    var actualValue = property.GetValue(actual, null);
                    if (expectedValue == null && actualValue == null)
                        continue;
                    Assert.AreEqual(expectedValue, actualValue);
                }
            }
        }
    }
}