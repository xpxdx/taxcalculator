﻿using AutoMapper;
using Calculator.Domain;
using System;

namespace Calculator.TaxJar
{
    public static class Mapping
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(c => 
            {
                c.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                c.AddProfile<MappingProfile>();
            });
            return config.CreateMapper();
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TaxJarLocationTaxRates, LocationTaxRates>();
            CreateMap<TaxJarOrderTax, OrderTax>();
        }
    }
}