﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace Calculator.TaxJar
{
    public class TaxJarConfiguration : ITaxJarConfiguration
    {
        IConfigurationSection appSettings;
        public TaxJarConfiguration()
        {
            var builder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "taxjar.appsettings.json");
            builder.AddJsonFile(path, false);
            var root = builder.Build();
            appSettings = root.GetSection("ApplicationSettings");
        }

        public string ApiToken => appSettings["ApiToken"];

        public string BaseUrl => appSettings["BaseUrl"];
    }
}
