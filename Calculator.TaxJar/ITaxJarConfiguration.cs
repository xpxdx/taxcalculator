﻿namespace Calculator.TaxJar
{
    public interface ITaxJarConfiguration
    {
        public string ApiToken { get; }

        public string BaseUrl { get; }
    }
}
