﻿using Calculator.Domain;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Calculator.TaxJar
{
    public class TaxJarTaxCalculator : ITaxCalculator
    {
        private readonly IRestClient client;
        private readonly ITaxJarConfiguration configuration;

        public TaxJarTaxCalculator(IRestClient client, ITaxJarConfiguration configuration)
        {
            this.client = client;
            this.configuration = configuration;
            this.client.BaseUrl = new Uri(this.configuration.BaseUrl);
        }

        public async Task<OrderTax> CalculateTaxAsync(Order order)
        {
            var json = JsonConvert.SerializeObject(
            new
            {
                from_country = order.From.Country,
                from_zip = order.From.Zip,
                from_state = order.From.State,
                from_city = order.From.City,
                from_street = order.From.Street,
                to_country = order.To.Country,
                to_zip = order.To.Zip,
                to_state = order.To.State,
                to_city = order.To.City,
                to_street = order.To.Street,
                amount = order.Amount,
                shipping = order.Shipping
            });

            var request = CreateRestRequest("taxes");
            request.AddJsonBody(json);

            var response = await client.ExecuteAsync(request, Method.POST, (new CancellationTokenSource()).Token);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var r = JObject.Parse(response.Content)["tax"].ToObject<TaxJarOrderTax>();
                return Mapping.Mapper.Map<OrderTax>(r);
            }

            return await Task.FromResult<OrderTax>(null);
        }

        public async Task<LocationTaxRates> GetTaxRatesAsync(Location location)
        {
            var request = CreateRestRequest($"rates/{ location.Zip }");
            request.AddParameter("city", location.City, ParameterType.QueryString);
            request.AddParameter("state", location.State, ParameterType.QueryString);
            request.AddParameter("country", location.Country, ParameterType.QueryString);
            request.AddParameter("street", location.Street, ParameterType.QueryString);

            var response = await client.ExecuteAsync(request, Method.GET, (new CancellationTokenSource()).Token);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var r = JObject.Parse(response.Content)["rate"].ToObject<TaxJarLocationTaxRates>();
                return Mapping.Mapper.Map<LocationTaxRates>(r);
            }
            
            return await Task.FromResult<LocationTaxRates>(null);
        }

        private RestRequest CreateRestRequest(string resource)
        {
            var request = new RestRequest(resource);
            request.AddHeader("Authorization", $"Token token=\"{ configuration.ApiToken }\" ");
            request.AddHeader("Content-Type", "application/json");
            return request;
        }
    }
}
