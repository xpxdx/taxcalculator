﻿using Newtonsoft.Json;

namespace Calculator.TaxJar
{
    internal sealed class TaxJarLocationTaxRates
    {
        [JsonProperty(PropertyName = "state_rate")]
        public decimal StateTaxRate { get; set; }

        [JsonProperty(PropertyName = "county_rate")]
        public decimal CountyTaxRate { get; set; }

        [JsonProperty(PropertyName = "city_rate")]
        public decimal CityTaxRate { get; set; }

        [JsonProperty(PropertyName = "combined_district_rate")]
        public decimal CombinedDistrictTaxRate { get; set; }

        [JsonProperty(PropertyName = "combined_rate")]
        public decimal CombinedTaxRate { get; set; }

        [JsonProperty(PropertyName = "freight_taxable")]
        public bool FreightTaxable { get; set; }
    }
}
