﻿using Newtonsoft.Json;

namespace Calculator.TaxJar
{
    internal sealed class TaxJarOrderTax
    {
        [JsonProperty(PropertyName = "amount_to_collect")]
        public decimal TaxCollectable { get; set; }

        [JsonProperty(PropertyName = "freight_taxable")]
        public bool FreightTaxable { get; set; }

        [JsonProperty(PropertyName = "order_total_amount")]
        public decimal OrderTotal { get; set; }

        [JsonProperty(PropertyName = "shipping")]
        public decimal Shipping { get; set; }

        [JsonProperty(PropertyName = "taxable_amount")]
        public decimal TaxableAmount { get; set; }

        [JsonProperty(PropertyName = "rate")]
        public decimal TaxRate { get; set; }

        [JsonProperty(PropertyName = "tax_source")]
        public string TaxSource { get; set; }
    }
}
